import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Register from './components/Registrasi/RegistrasiCard'
import RegisterView from './components/Registrasi/RegisterView'
import RegistrasiLevel1 from './components/Registrasi/RegistrasiLevel1'
import RegistrasiLevel2 from './components/Registrasi/RegistrasiLevel2'
import RegistrasiLevel3 from './components/Registrasi/RegistrasiLevel3'
import RegisterModal from './components/Registrasi/RegisterModal'
import Profil from './components/Profile/EditPasswordProfile'
import ProfilView from './components/Profile/ProfileView'

import AddGroup from './components/Group/AddGroup'
import GroupDasboard from './components/Group/GroupDashboard'

import Home from './components/home/Home'
import Login from './components/Login/Login'
import Logout from './components/Login/Logout'
import Dashboard from './components/Dashboard/Dashboard'
import NoMatch from './components/error/Error404'

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/register">
          <Register />
        </Route>
        <Route exact path="/registrasiview">
          <RegisterView />
        </Route>
        <Route exact path="/registration-step-1">
          <RegistrasiLevel1 />
        </Route>
        <Route exact path="/registration-step-2">
          <RegistrasiLevel2 />
        </Route>
        <Route exact path="/registration-finish">
          <RegistrasiLevel3 />
        </Route>

        <Route exact path="/profile">
          <Profil />
        </Route>
        <Route exact path="/profileview">
          <ProfilView />
        </Route>
        <Route exact path="/group">
          <AddGroup/>
        </Route>
        <Route exact path='/groupdashboard'>
          <GroupDasboard/>
        </Route>

        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/user/password/new">
          <RegisterModal />
        </Route>
        <Route exact path="/logout">
          <Logout />
        </Route>        
        <Route exact path="/">
          <Home />
        </Route>

        <Route exact path="/dashboard">
          <Dashboard />
        </Route>
          
        <Route path='*'>
          <NoMatch />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;