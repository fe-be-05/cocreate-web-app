import { faNewspaper, faProjectDiagram, faUser } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { Component } from 'react'
import profile from '../../assets/image/profile.png'
import './MenuKiri.css'

export default class MenuKiri extends Component {
    render() {
        return (
            <div>
                <div className="container CostumContainerMenuKiri">
                    <div className="card text-center">
                        <img src={profile} class="my-2 card-img-top costumImgProfileMenukiri" alt="profile" />
                        <div className="card-body">
                            <div>
                                <button className="btn btn-danger text-left costumBtnMenuKiri"> 
                                <FontAwesomeIcon className="mr-2" icon={faNewspaper}></FontAwesomeIcon>
                                Artikel</button>
                            </div>
                            <div>

                                <button className="btn btn-danger text-left costumBtnMenuKiri">
                                    <FontAwesomeIcon className="mr-2" icon={faUser}></FontAwesomeIcon>
                                     Forum</button>
                            </div>
                            <div>
                                <button className="btn btn-danger text-left costumBtnMenuKiri">
                                    <FontAwesomeIcon className="mr-2" icon={faProjectDiagram}></FontAwesomeIcon>
                                    Proyek Inovasi PI</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}