import React, { Component } from 'react'
import './Kategory.css'

export default class Kategory extends Component{
    render() {
        return (
            <div>
                <div class="card container-card-Kategory-margin">
                    <div class="card-header text-Kategory">
                        Featured
                      </div>
                    <ul class="list-group list-group-flush">
                    <div>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Keuangan</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Otomotif</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Teknologi Informasi / IT</button>
                       
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Sosial</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Olahraga</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Sains</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Desain</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Bisnis dan Manajemen</button>
                       
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Kewirausahan</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Kesehatan</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Energi dan Lingkungan</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Hukum dan Advokasi</button>
                        </div>
                    </ul>
                    
                </div>
            </div>
        )
    }
}