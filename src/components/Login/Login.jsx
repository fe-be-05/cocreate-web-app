import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import GoogleLogin from 'react-google-login'

import './Login.css';
import LoginUndraw from '../../assets/image/login-undraw.png';
import Google from '../../assets/image/google.png';
import Facebook from '../../assets/image/facebook-logo-3.webp';
import Navbar_Login_Regis from '../navbar/Navbar_Login_Regis';


class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            loginSuccess: false,
        }
        this._handleSubmit = this._handleSubmit.bind(this)
        this._handleGoogleLogin = this._handleGoogleLogin.bind(this)
        this._handleFacebookLogin = this._handleFacebookLogin.bind(this)
        this._handleEmailChange = this._handleEmailChange.bind(this)
        this._handlePasswordChange = this._handlePasswordChange.bind(this)
    }

    componentDidMount() {
        if (!this.props.isLoggedIn) {
            document.getElementById("btn-login").disabled = true
            this._enableLoginButton()
        }
    }

    _enableLoginButton() {
        const email = document.getElementById('input-email').value
        const password = document.getElementById('input-password').value
        if (email.length > 0 && password.length > 0) {
            document.getElementById("btn-login").disabled = false
        } else {
            document.getElementById("btn-login").disabled = true
        }
    }

    _handleEmailChange(event) {
        this.setState({
            email: event.target.value
        })
        this._enableLoginButton()
    }

    _handlePasswordChange(event) {
        this.setState({
            password: event.target.value
        })
        this._enableLoginButton()
    }

    async _handleSubmit(event) {
        event.preventDefault()
        if (!this.state.email || !this.state.password) {
            alert('Email dan password harus diisi.')
        } else {
            console.log('doing login for ' + this.state.email)

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            var raw = JSON.stringify({ "email": this.state.email, "password": this.state.password });

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };

            const token = await fetch("http://kelompok5.dtstakelompok1.com/login", requestOptions)
                .then(response => response.json())
                .then(result => result.token)
                .catch(error => console.log('error', error));

            myHeaders.append("Authorization", token);

            requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };

            const account = await fetch("http://kelompok5.dtstakelompok1.com/account", requestOptions)
                .then(response => response.json())
                .then(result => result.account)
                .catch(error => console.log('error', error));

            if (account != null) {
                console.log('Akun terdeteksi: ' + account.email)
                this.props.dispatch({
                    type: 'LOGIN',
                    payload: { user: account, token: token }
                })

                this.setState({
                    loginSuccess: true
                })
            } else {
                alert('Email atau password salah')
            }
        }
    }

    async _handleGoogleLogin() {

        console.log('doing login for ' + this.state.email)

        var myHeaders = new Headers();

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow',
        };

        const token = await fetch("https://kelompok5.dtstakelompok1.com/login/google", requestOptions)
            .then(response => console.log(response))
            .then(result => result.token)
            .catch(error => console.log('error', error));

        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", token);

        requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        const account = await fetch("http://kelompok5.dtstakelompok1.com/account", requestOptions)
            .then(response => response.json())
            .then(result => result.account)
            .catch(error => console.log('error', error));

        if (account != null) {
            console.log('Akun terdeteksi: ' + account.email)
            this.props.dispatch({
                type: 'LOGIN',
                payload: { user: account, token: token }
            })

            this.setState({
                loginSuccess: true
            })
        } else {
            alert('Terjadi kesalahan')
        }
    }

    async _handleFacebookLogin() {
        console.log('doing login for ' + this.state.email)

        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        const token = await fetch("http://kelompok5.dtstakelompok1.com/login/facebook", requestOptions)
            .then(response => response.json())
            .then(result => result.token)
            .catch(error => console.log('error', error));

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", token);

        requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        const account = await fetch("http://kelompok5.dtstakelompok1.com/account", requestOptions)
            .then(response => response.json())
            .then(result => result.account)
            .catch(error => console.log('error', error));

        if (account != null) {
            console.log('Akun terdeteksi: ' + account.email)
            this.props.dispatch({
                type: 'LOGIN',
                payload: { user: account, token: token }
            })

            this.setState({
                loginSuccess: true
            })
        } else {
            alert('Terjadi kesalahan')
        }
    }

    async _responseGoogle(response) {
        console.log(response)
        var name = response.profileObj.name
        var email = response.profileObj.email
        var body = JSON.stringify({name: name, email: email})
        console.log(body)
        var r = await fetch('http://kelompok5.dtstakelompok1.com/login/google/add', {
            method: 'POST',
            body: body
        })
        console.log(r)
    }

    async _responseFacebook(response) {
        var accessToken = response.accessToken
        var response = await fetch(`https://graph.facebook.com/me?fields=id,name,email&access_token=${accessToken}`)
        console.log(response)
    }

    render() {
        if (this.state.loginSuccess || this.props.isLoggedIn) {
            return <Redirect to="/dashboard" />
        }
        return (
            <React.Fragment>
                <Navbar_Login_Regis/>
                <div id="section" >
                    <img className="wive" src={LoginUndraw} alt="ilustrasi-login" />
                    <div className="card o-hidden border-2  shadow-lg my-5">
                        <div className="card-body-login  p-0">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="p-5">
                                        <h2>Log in</h2>
                                        <br />
                                        <form onSubmit={this._handleSubmit}>
                                            <h6>Email</h6>
                                            <div className="form-group">
                                                <input id="input-email" type="email" className="form-control" placeholder="Email" onChange={this._handleEmailChange} />
                                            </div>
                                            <h6>Password</h6>
                                            <div className="form-group">
                                                <input id="input-password" type="password" className="form-control" placeholder="Password" onChange={this._handlePasswordChange} />
                                            </div>
                                            <div className="form-group">
                                                <div className="custom-control custom-checkbox small">
                                                    <input type="checkbox" className="custom-control-input" id="customCheck" />
                                                    <label className="custom-control-label " htmlFor="customCheck">Remember Me</label>
                                                    <a href="#" className="label-anchor" >Lupa Password?</a>
                                                </div>
                                            </div>
                                            <button id="btn-login" type="submit" className="btn login ml-2" onClick={this._handleSubmit}>Log in</button>

                                        </form>
                                        <br />
                                        <span>Belum Punya Akun?<span ><a href="/register" className="regist"> Daftar</a></span></span>
                                        <hr />
                                        <h5>Log in dengan</h5>

                                        <div className="logo" >
                                            <GoogleLogin
                                                clientId="251751587800-rh1fg90aj7pffllmmba33msir32376lu.apps.googleusercontent.com"
                                                render={renderProps => (
                                                    <button onClick={renderProps.onClick} className="btn btn-light ml-2"><img src={Google} className="logo-google" alt="tombol-google" /> Google</button>
                                                )}
                                                buttonText="Login"
                                                onSuccess={this._responseGoogle}
                                                onFailure={this._responseGoogle}
                                                cookiePolicy={'single_host_origin'}
                                            />

                                            <FacebookLogin
                                                appId="655749328443697"
                                                callback={this._responseFacebook}
                                                render={renderProps => (
                                                    <button onClick={renderProps.onClick} className="btn btn-light ml-2 "><img src={Facebook} className="logo-fb" alt="tombol-facebook" /> Facebook</button>
                                                )}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <br />


                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return { isLoggedIn: state.isLoggedIn }
}


const mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)
