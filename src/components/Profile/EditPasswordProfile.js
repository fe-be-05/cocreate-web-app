import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faIdCard, faPhone, faUserCircle, faUserLock, faVenusMars } from '@fortawesome/free-solid-svg-icons';
//import { faFacebookF, faGoogle, faTwitterSquare } from '@fortawesome/free-brands-svg-icons';
import './Profile.css';
import profile from '../../assets/image/registrasi.png'
import { Link } from 'react-router-dom';
import NavbarDashboard from '../navbar/NavbarDashboard';

export default class ProfileCard extends Component {
    constructor() {
        super(); {
            this.state = {
                usernameProfile: '',
                notelephoneProfile: '',
                jeniskelaminProfile: '',
                alamatProfile: '',
                emailProfile: ''
            }
        }
        this._handleUserName = this._handleUserName.bind(this)
        this._handleEmailChange = this._handleEmailChange.bind(this)
        this._handlePasswordChange = this._handlePasswordChange.bind(this)
        this._handleTelephone = this._handleTelephone.bind(this)
        this._handleJeniskelamin = this._handleJeniskelamin.bind(this)
        this._handleAlamat = this._handleAlamat.bind(this)

        this.saveDataProfileToAPI = this.saveDataProfileToAPI.bind(this)
    }

    _handleUserName(event) {
        this.setState({
            usernameProfile: event.target.value
        })
    }

    _handleTelephone(event) {
        this.setState({
            notelephoneProfile: event.target.value
        })
    }

    _handleJeniskelamin(event) {
        this.setState({
            jeniskelaminProfile: event.target.value
        })
    }

    _handleAlamat(event) {
        this.setState({
            alamatProfile: event.target.value
        })
    }

    _handleEmailChange(event) {
        this.setState({
            emailProfile: event.target.value
        })
    }

    _handlePasswordChange(event) {
        this.setState({
            passwordProfile: event.target.value
        })
    }

    saveDataProfileToAPI(event) {
        var axios = require('axios');
        var data = JSON.stringify({ "photo": "gambar.jpg", "gender": "Laki-Laki", "address": "jln pahlawan no 1" });

        var config = {
            method: 'put',
            url: 'http://kelompok5.dtstakelompok1.com/account/update',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJ1c2VyQGdtYWlsLmNvbSJ9.7cRfP6ACg_Y3-0cxgqTk8S33zlcsqVL9PTWjBPF53pk'
            },
            fullname: this.state.usernameProfile,
            email: this.state.emailProfile,
            handphone: this.state.notelephoneProfile,
            gender: this.state.jeniskelaminProfile,
            address: this.state.alamatProfile

        };

        axios(config)
            .then(function (response) {
                //console.log(JSON.stringify(response.data));
                console.log("data api", response)

            })
            .catch(function (error) {
                console.log("erorr memanggil API", error);
            });
        console.log("data yang di kirim", config)
    }
    render() {
        return (
            <div>
                <NavbarDashboard />
                <div className="spasiAtas">
                    <div className="row justify-content-center">
                        <div className="imgcontainer">
                            <img className="contentimg" src={profile} />
                        </div>
                        <div className="card bg-light my-3 ">
                            <div className="card-header formku">Profil anda</div>
                            <div className="card-body">
                                <form method="post" onSubmit={(event) => this.saveDataProfileToAPI(event)}>
                                    <div className="form-group">
                                        <label for="Identitas">
                                            <FontAwesomeIcon icon={faIdCard} /> No. Identitas
                                        </label>
                                        <input type="text" className="form-control" placeholder="Masukan No. Identitas anda" />
                                    </div>
                                    <div className="form-group">
                                        <label for="Username">
                                            <FontAwesomeIcon icon={faUserCircle} /> Username
                                        </label>
                                        <input type="text" id="usernameProfile" className="form-control" placeholder="Masukan Username anda"
                                            onChange={this._handleUserName} />
                                    </div>
                                    <div className="form-group">
                                        <label for="telepon">
                                            <FontAwesomeIcon icon={faPhone} /> No. Telepon </label>
                                        <input type="text" id="notelepohoneProfile" className="form-control" placeholder="Masukan No. Telepon anda"
                                            onChange={this._handleTelephone} />
                                    </div>

                                    <label><FontAwesomeIcon icon={faVenusMars} /> Jenis Kelamin </label>
                                    <div className="form-check">
                                        <label className="form-check-label" for="radio1">
                                            <input type="radio" className="form-check-input" value="jeniskelaminProfile" id="radio1" name="optradio" value="option1" checked
                                                onChange={this._handleJeniskelamin} />Laki Laki
                                </label>
                                    </div>
                                    <div className="form-check">
                                        <label className="form-check-label" for="radio2">
                                            <input type="radio" className="form-check-input" value="jeniskelaminProfile" id="radio2" name="optradio" value="option2"
                                                onChange={this._handleJeniskelamin} />Wanita
                                        </label>
                                    </div>
                                    <br />
                                    <div className="form-group">
                                        <label for="comment"><FontAwesomeIcon icon={faHome} /> Alamat:</label>
                                        <textarea className="form-control" rows="5" id="alamatProfile"
                                            onChange={this._handleAlamat}></textarea>
                                    </div>
                                    <div className="form-group">
                                        <button type="button" className="btn btn-dark" data-toggle="modal" data-target="#exampleModalCenter">
                                            <FontAwesomeIcon icon={faUserLock}></FontAwesomeIcon>  Ganti Password
                                        </button>

                                        <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div className="modal-dialog modal-dialog-centered" role="document">
                                                <div className="modal-content">
                                                    <div className="modal-header">
                                                        <h5 className="modal-title" id="exampleModalLongTitle">Ganti Password</h5>
                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div className="modal-body">
                                                        <form>
                                                            <div className="form-group">
                                                                <label for="Password" className="col-form-label">Password</label>
                                                                <input type="text" className="form-control" id="Password" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label for="Password Baru" className="col-form-label">Password Baru</label>
                                                                <input type="text" className="form-control" id="Password Baru" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label for="Ulangi Password" className="col-form-label">Ulangi Password</label>
                                                                <input type="text" className="form-control" id="Ulangi Password" />
                                                            </div>

                                                        </form>
                                                    </div>
                                                    <div className="modal-footer">
                                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                                        <button type="button" className="btn btn-primary">Simpan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" className="btn btn-danger btn-lg btn-block my-5"> <Link className="ProfilBtn" to="/kontent"> Simpan </Link></button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}