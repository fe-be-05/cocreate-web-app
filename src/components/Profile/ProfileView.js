import React, { Component } from 'react';
import './ProfileView.css';
import profileimg from '../../assets/image/profile.png';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import NavbarDashboard from '../navbar/NavbarDashboard';
import axios from 'axios'
// import { connect } from 'react-redux';
// import { Redirect } from 'react-router-dom';

export default class ProfileView extends Component {
    constructor() {
        super(); {
            this.state = {
                profilUser: []
            }
        }
    }
    componentDidMount() {
        var axios = require('axios');

        var config = {
            method: 'get',
            url: 'http://kelompok5.dtstakelompok1.com/account',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJ1c2VyQGdtYWlsLmNvbSJ9.7cRfP6ACg_Y3-0cxgqTk8S33zlcsqVL9PTWjBPF53pk'
            }
        };

        axios(config)
            .then(response => {
                //const profilUser = response.data.account
                //console.log(JSON.stringify(response.data));
                console.log("data api", response.data.account)
                this.setState({
                    profilUser: response.data.account
                })

            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return (
            <div>
                <NavbarDashboard/>
                <div className="container">
                    <div className="row headerHeight">
                        <div className="col-md">
                            <div className="card">
                                <div className="card-header">
                                    Profile User
                            </div>
                                <div className="card-body">
                                    <div className="cardView">
                                        <img src={profileimg} alt="John" className="ukuran" />
                                        <h1>John Doe</h1>
                                        <p className="titleView">CEO & Founder, Example</p>
                                        <p>Harvard University</p>
                                        <a href="#"><i className="fa fa-dribbble"></i></a>
                                        <a href="#"><i className="fa fa-twitter"></i></a>
                                        <a href="#"><i className="fa fa-linkedin"></i></a>
                                        <a href="#"><i className="fa fa-facebook"></i></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-md">
                            <div className="card-body">
                                <h5 className="card-title">Biodata User</h5>
                                <hr />
                                <table className="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col"></th>
                                            <th scope="col"></th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row"></th>
                                            <td>Nama</td>
                                            <td></td>
                                            <td>{this.state.profilUser.name}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row"></th>
                                            <td>Email</td>
                                            <td></td>
                                            <td>{this.state.profilUser.email}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row"></th>
                                            <td>Tanggal Lahir</td>
                                            <td></td>
                                            <td>1-09-1298</td>
                                        </tr>

                                        <tr>
                                            <th scope="row"></th>
                                            <td>Alamat</td>
                                            <td></td>
                                            <td>Jl. Anu kab. itu, kec.disana sedikit, desa jauh jauh lagi</td>
                                        </tr>
                                        <tr>
                                            <th scope="row"></th>
                                            <td>No. Telepon</td>
                                            <td></td>
                                            <td>{this.state.profilUser.handphone}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br />
                                <button className="btn btn-primary"><Link className="profileBtnEdit" to="/profile"> + Edit data</Link></button>
                                <hr />
                            </div>
                        </div>
                    </div>
                    <button className="btn btn-secondary"><Link className="profileBtnEdit" to="/kontent"><FontAwesomeIcon icon={faHome}></FontAwesomeIcon>Home </Link></button>
                </div>
            </div>
        )
    }
}