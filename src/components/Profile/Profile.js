import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAddressBook, faHome, faIdCard, faPhone, faPhoneSquare, faTransgender, faTransgenderAlt, faUserCircle, faUserLock, faVenusMars } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF, faGoogle, faTwitterSquare } from '@fortawesome/free-brands-svg-icons';
import './Profile.css';


export default class Profile extends Component {
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <h2 className="fontku">Profile</h2>
                        <form>
                            <div className="form-group">
                                <label for="Identitas">
                                    <FontAwesomeIcon icon={faIdCard} /> No. Identitas
                                </label>
                                <input type="email" className="form-control" placeholder="Masukan No. Identitas anda" required />
                            </div>
                            <div className="form-group">
                                <label for="Username">
                                    <FontAwesomeIcon icon={faUserCircle} /> Username
                                </label>
                                <input type="email" className="form-control" placeholder="Masukan Username anda" required />
                            </div>
                            <div className="form-group">
                                <label for="telepon">
                                    <FontAwesomeIcon icon={faPhone} /> No. Telepon </label>
                                <input type="email" className="form-control" placeholder="Masukan No. Telepon anda" required />
                            </div>

                            <label><FontAwesomeIcon icon={faVenusMars} /> Jenis Kelamin </label>
                            <div class="form-check">
                                <label class="form-check-label" for="radio1">
                                    <input type="radio" class="form-check-input" id="radio1" name="optradio" value="option1" checked />Laki Laki
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label" for="radio2">
                                    <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2" />Wanita
                                </label>
                            </div>
                            <br />
                            <div class="form-group">
                                <label for="comment"><FontAwesomeIcon icon={faHome} /> Alamat:</label>
                                <textarea class="form-control" rows="5" id="comment"></textarea>
                            </div>
                            <div className="form-group">
                                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModalCenter">
                                <FontAwesomeIcon icon={faUserLock}></FontAwesomeIcon>  Ganti Password
                                </button>

                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Ganti Password</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    <div class="form-group">
                                                        <label for="Password" class="col-form-label">Password</label>
                                                        <input type="text" class="form-control" id="Password" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Password Baru" class="col-form-label">Password Baru</label>
                                                        <input type="text" class="form-control" id="Password Baru" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Ulangi Password" class="col-form-label">Ulangi Password</label>
                                                        <input type="text" class="form-control" id="Ulangi Password" />
                                                    </div>

                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                                <button type="button" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary btn-lg btn-block my-5"> Simpan </button>

                        </form>

                    </div>
                </div>
            </div>
        );
    }
}
