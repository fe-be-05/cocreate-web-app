import { faTelegramPlane } from '@fortawesome/free-brands-svg-icons'
import { faCommentAlt, faCommentDollar, faCommentDots, faCommentMedical, faImage, faThumbsUp } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { Component } from 'react'
import iconupload from '../../assets/image/upload-picture-icon.jpg'
import './Posting.css'
export default class Posting extends Component {
    render() {
        return (
            <div>
                <div className="card my-4">
                    <div className="card-body text-secondary">
                        <div className="form-group">
                            <label for="exampleFormControlTextarea1">Tulis sesuatu...</label>
                            <textarea className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            <div className="image-upload">
                                <label for="file-input">
                                    <img className="iconUploadImg" src={iconupload} />
                                </label>
                                <input id="file-input" type="file" className="CostumInput" />
                            </div>
                        </div>
                    </div>


                    <div className="card-footer bg-transparent border-white">
                        <div className="text-right">

                            <button type="button" className="btn btn-secondary mx-2" data-toggle="modal" data-target="#staticBackdrop">
                                Kategory
                            </button>

                            <div className="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div className="modal-dialog modal-lg modal-dialog-centered">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h5 className="modal-title" id="staticBackdropLabel">Modal title</h5>
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div className="modal-body text-left">
                                            <div className="container-fluid">
                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <div class="form-check">
                                                            <label class="form-check-label" for="check1">
                                                                <input type="checkbox" class="form-check-input" id="check1" name="option1" value="something" />Teknologi Informasi IT</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <label class="form-check-label" for="check2">
                                                                <input type="checkbox" class="form-check-input" id="check2" name="option2" value="something" />Otomotif</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                <input type="checkbox" class="form-check-input" />Keungan</label>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-4">
                                                    <div class="form-check">
                                                            <label class="form-check-label" for="check1">
                                                                <input type="checkbox" class="form-check-input" id="check1" name="option1" value="something" />Sosial</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <label class="form-check-label" for="check2">
                                                                <input type="checkbox" class="form-check-input" id="check2" name="option2" value="something" />Olahraga</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                <input type="checkbox" class="form-check-input" />Keungan</label>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-4">
                                                    <div class="form-check">
                                                            <label class="form-check-label" for="check1">
                                                                <input type="checkbox" class="form-check-input" id="check1" name="option1" value="something" />Sains</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <label class="form-check-label" for="check2">
                                                                <input type="checkbox" class="form-check-input" id="check2" name="option2" value="something" />Desain</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                <input type="checkbox" class="form-check-input" />Kesehatan</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button className="btn btn-primary"><FontAwesomeIcon icon={faTelegramPlane}></FontAwesomeIcon> Kirim</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}