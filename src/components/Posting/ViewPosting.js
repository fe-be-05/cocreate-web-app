import { faComment, faCommentAlt, faShare, faThumbsUp } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { Component } from 'react'
import inovatif1 from '../../assets/image/inovatif1.jpg'
import inovatif2 from '../../assets/image/inovatif2.jpg'
import inovatif3 from '../../assets/image/inovatif3.jpg'
import profile from '../../assets/image/profile.png'
import './Posting.css'
export default class ViewPosting extends Component {
    render() {
        return (
            <div>
                <div className="card my-5">
                    <div className="row CostumPosting">
                        <div className="col col-md-2 costumImgPostingProfile">
                            <img src={profile} className="imgPostingProfile" />
                        </div>
                        <div className="col costumTextJudul">
                            <h5 className="card-title">Card title</h5>
                            <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                    </div>
                    <img src={inovatif1} className="card-img-top cardPostingCostum" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">Card title</h5>
                        <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
                        <hr />
                        <a href="#" className="aMargin"><FontAwesomeIcon icon={faThumbsUp}></FontAwesomeIcon><small>25</small></a>
                        <a className="aMargin" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                            <FontAwesomeIcon icon={faCommentAlt}></FontAwesomeIcon>
                            <small>25</small>
                        </a>
                        <a href="#" className="aMargin"><FontAwesomeIcon icon={faShare}></FontAwesomeIcon></a>

                        <div className="collapse my-2" id="collapseExample">
                            <div className="card card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                <div className="form-group">
                                    <label for="exampleFormControlTextarea1">Example textarea</label>
                                    <textarea className="form-control" id="exampleFormControlTextarea1" rows="1"></textarea>
                                </div>
                            </div>
                            <div className="text-right mt-2">
                                <button className="btn btn-primary">Kirim</button>
                            </div>
                        </div>

                    </div>
                </div>

                <div className="card my-5">
                    <div className="row CostumPosting">
                        <div className="col col-md-2 costumImgPostingProfile">
                            <img src={profile} className="imgPostingProfile" />
                        </div>
                        <div className="col">
                            <h5 className="card-title">Card title</h5>
                            <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                    </div>
                    <img src={inovatif2} className="card-img-top cardPostingCostum" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">Card title</h5>
                        <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
                        <hr />
                        <a href="#" className="aMargin"><FontAwesomeIcon icon={faThumbsUp}></FontAwesomeIcon><small>25</small></a>
                        <a href="#" className="aMargin"><FontAwesomeIcon icon={faCommentAlt}></FontAwesomeIcon><small>120</small></a>
                        <a href="#" className="aMargin"><FontAwesomeIcon icon={faShare}></FontAwesomeIcon></a>
                    </div>
                </div>

                <div className="card my-5">
                    <div className="row CostumPosting">
                        <div className="col col-md-2 costumImgPostingProfile">
                            <img src={profile} className="imgPostingProfile" />
                        </div>
                        <div className="col">
                            <h5 className="card-title">Card title</h5>
                            <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                    </div>
                    <img src={inovatif3} className="card-img-top cardPostingCostum" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">Card title</h5>
                        <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
                        <hr />
                        <a href="#" className="aMargin"><FontAwesomeIcon icon={faThumbsUp}></FontAwesomeIcon><small>25</small></a>
                        <a href="#" className="aMargin"><FontAwesomeIcon icon={faCommentAlt}></FontAwesomeIcon><small>120</small></a>
                        <a href="#" className="aMargin"><FontAwesomeIcon icon={faShare}></FontAwesomeIcon></a>
                    </div>
                </div>

            </div>
        )
    }
}