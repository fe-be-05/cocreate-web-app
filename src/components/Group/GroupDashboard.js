import React, { Component } from 'react'
import './Group.css'
import profile from '../../assets/image/profile.png'
import NavbarDasboard from '../navbar/NavbarDashboard'

export default class GroupDashboard extends Component {
    render() {
        return (
            <div>
                <NavbarDasboard />
                <div className="my-5">

                </div>

                <div className="row">
                    {/* ---------- Daftar Anggota --------- */}
                    <div className="col col-md-3 customCardMarginGroupDasboard">
                        <div className="card">
                            <div className="card-header">
                                <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Daftar Anggota
                                </a>
                            </div>
                            <div className="collapse" id="collapseExample">
                                <div className="card card-body">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">
                                            <button type="button" className="btn btn-primary customGroupDashboardAddAnggota" data-toggle="modal" data-target="#exampleModal">
                                                Tambah Anggota +</button>
                                        </li>
                                        <li className="list-group-item">Gotfried</li>
                                        <li className="list-group-item">Muh Aswar</li>
                                        <li className="list-group-item">Aisyah</li>
                                        <li className="list-group-item">Apriansyah</li>
                                    </ul>
                                </div>
                            </div>
                            {/* -------- Modal Tambah Anggota ----- */}
                            <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div className="modal-dialog modal-dialog-centered">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h5 className="modal-title" id="exampleModalLabel">Tambahkan Anggota Group</h5>
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div className="modal-body">
                                            <label for="inputAnggotaModal"> Cari Anggota</label>
                                            <input type="text" className="form-control customInputTugasGroupDashboard" id="inputAnggotaModal" />
                                        </div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" className="btn btn-primary">Tambah</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* ------ Tugas ------- */}
                        <div className="card">
                            <div className="card-header">
                                <a data-toggle="collapse" href="#collapseExampleTugas" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Daftar Tugas
                                </a>
                            </div>
                            <div className="collapse" id="collapseExampleTugas">
                                <div className="card card-body">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Pembuatan Artikel</li>
                                        <li className="list-group-item">Pembuatan Dashboard</li>
                                        <li className="list-group-item">Login dan Registrasi</li>
                                        <li className="list-group-item">Lofi dan Hifi</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <label for="inputTugas"> Buat Tugas</label>
                        <input type="text" className="form-control customInputTugasGroupDashboard" id="inputTugas" />
                        <button className="btn btn-success">Buat</button>
                        <div className="card mt-4 mb-2">
                            <div className="card-header">Target</div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="card">
                                    <div className="card-header">
                                        <strong>Hari</strong>
                                    </div>
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Cras justo odio</li>
                                        <li className="list-group-item">Dapibus ac facilisis in</li>
                                        <li className="list-group-item">Vestibulum at eros</li>
                                    </ul>
                                </div>
                            </div>

                            {/* ----Target Mingguan --- */}
                            <div className="col">
                                <div className="card">
                                    <div className="card-header">
                                       <strong> Minggu</strong>
                                    </div>
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Cras justo odio</li>
                                        <li className="list-group-item">Dapibus ac facilisis in</li>
                                        <li className="list-group-item">Vestibulum at eros</li>
                                    </ul>
                                </div>
                            </div>

                            {/* ----- Target Bulan ---- */}
                            <div className="col">
                                <div className="card">
                                    <div className="card-header">
                                        <strong>Bulan</strong>
                                    </div>
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Cras justo odio</li>
                                        <li className="list-group-item">Dapibus ac facilisis in</li>
                                        <li className="list-group-item">Vestibulum at eros</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        {/*-------- Progress --------- */}
                        <div className="card mt-5 mb-2">
                            <div className="card-header">Pencapaian</div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="card">
                                    <div className="card-header">
                                        <strong>Hari</strong>
                                    </div>
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Cras justo odio</li>
                                        <li className="list-group-item">Dapibus ac facilisis in</li>
                                        <li className="list-group-item">Vestibulum at eros</li>
                                    </ul>
                                </div>
                            </div>

                            {/* ----Progress Mingguan --- */}
                            <div className="col">
                                <div className="card">
                                    <div className="card-header">
                                        <strong>Minggu</strong>
                                    </div>
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Cras justo odio</li>
                                        <li className="list-group-item">Dapibus ac facilisis in</li>
                                        <li className="list-group-item">Vestibulum at eros</li>
                                    </ul>
                                </div>
                            </div>

                            {/* ----- Progress Bulan ---- */}
                            <div className="col">
                                <div className="card">
                                    <div className="card-header">
                                        <strong>Bulan</strong>
                                    </div>
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Cras justo odio</li>
                                        <li className="list-group-item">Dapibus ac facilisis in</li>
                                        <li className="list-group-item">Vestibulum at eros</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div className="col col-md-1">

                    </div>
                </div>
            </div>
        )
    }
}