import React, { Component } from 'react'

import Navbar from '../navbar/NavbarDashboard'
import './Group.css'
import profile from '../../assets/image/profile.png'
import outputLogo from '../../assets/image/output.png'
import { Link } from 'react-router-dom'
export default class AddGroup extends Component {
    render() {
        return (
            <div>
                <Navbar/>
                <div className="container my-5">
                    <div className="row">
                        <div className="col customImgLayout">
                            <div>
                                <img src={outputLogo} className="costumImgGroupAdd" />
                            </div>
                        </div>
                        <div className="col">
                            <h3>Buat Group Proyek Inovasi (PI)</h3>
                            <p>Buat semua orang bekerja di satu tempat dengan menambahkan mereka ke tim</p>

                            <div className="customFormGroup">
                                <form>
                                    <label className="customTextGroup">Nama Group</label>
                                    <input className="form-control form-control-lg customInputGroup" type="text" placeholder="Nama Group"></input>
                                    <label className="customTextGroup">Deskripsi Group</label>
                                    <input className="form-control form-control-lg customInputGroup" type="text" placeholder="Deskripsi"></input>
                                    <label className="customTextGroup">Undang Teman Ke Group ini</label>
                                    <input className="form-control form-control-lg customInputGroup" type="text" placeholder="ID Tim"></input>

                                    <button className="btn btn-danger"><Link className="customBtnGroup" to='/groupdashboard'> Buat Group </Link> </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}