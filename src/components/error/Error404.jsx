import React, { Component } from 'react'
import "./Error404.css"
import error1 from '../../assets/image/error.png';
import Navbar_Login_Regis from '../navbar/Navbar_Login_Regis';

export default class Error404 extends Component {
    render() {
        return (
            <div>
                <Navbar_Login_Regis/>
                <div className="container m-auto text-center">
                    <img src={error1} style={{marginTop: "60px"}}/>
                    <h2 className="styling">Page Not Found</h2>
                    <p className="styling">Halaman yang kamu cari tidak ditemukan</p>
                    <a href='/' role="button" className="btn btn-small">Home</a>
                </div>
            </div>
        )
    }
}
