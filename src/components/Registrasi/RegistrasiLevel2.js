import React, { Component } from 'react'
import { ProgressBar } from 'react-bootstrap'
import './Registrasi.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import Navbar_Login_Regis from '../navbar/Navbar_Login_Regis';

export default class RegistrasiLevel2 extends Component {
    render() {
        return (
            <div>
                <Navbar_Login_Regis/>
                <div className="container my-5">
                    <div>
                        <h5 className="TextHeaderForm">Lengkapi form registrasi anggota</h5>
                        <p className="textNote"> Form ini akan membantu admin dalam verifikasi keanggotaan </p>
                        <div className="form-group row">
                            <label for="namaLengkap" className="col-sm-2 col-form-label">Nama Lengkap </label>
                            <div className="col-sm-10">
                                <input type="email" className="form-control" id="namaLengkap" placeholder="Nama Lengkap" />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label for="noTelepon" className="col-sm-2 col-form-label">No. Telepon </label>
                            <div className="col-sm-10">
                                <input type="email" className="form-control" id="noTelepon" placeholder="Email" />
                            </div>
                        </div>

                        <div className="row">
                            <label className="col-form-label col-sm-2 pt-0">Jenis Kelamin</label>
                            <div className="col-sm-10">
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked />
                                    <label className="form-check-label" for="gridRadios1">
                                        Laki Laki
                                            </label>
                                </div>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2" />
                                    <label className="form-check-label" for="gridRadios2">
                                        Perempuan
                                            </label>
                                </div>
                            </div>
                        </div>

                        <div className="form-group">
                            <label for="exampleFormControlTextarea1">Alamat Lengkap</label>
                            <textarea className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>

                        <div className="form-group row">
                            <div className="col-sm-6">
                                <button type="submit" className="btn btn-secondary"><Link className="btnKembaliRegistrasi" to="/registration-step-1">Kembali</Link></button>
                            </div>
                            <div className="col-sm-6 text-right">
                                <button type="submit" className="btn btn-danger rounded"><Link className="btnKembaliRegistrasi" to="/registration-finish"> Selanjutnya </Link></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}