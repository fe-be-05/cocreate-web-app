import { faBan, faCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import NavbarDashboard from '../navbar/NavbarDashboard';

export default class RegistrasiAdmin extends Component {
    constructor() {
        super();
        this.state = {
            person: [],
            ids: [],
            isGoing: true,
            stringku:"id"
        }
        this._handleCekBox = this._handleCekBox.bind(this)
        this._terimaBanyak = this._terimaBanyak.bind(this)
        this._deleteBanyak = this._deleteBanyak.bind(this)
    }

    componentDidMount() {
        var axios = require('axios');

        var config = {
            method: 'get',
            url: 'http://kelompok5.dtstakelompok1.com/admin/account',
            headers: {
                'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJ1c2VyQGdtYWlsLmNvbSJ9.7cRfP6ACg_Y3-0cxgqTk8S33zlcsqVL9PTWjBPF53pk'
            }
        };

        axios(config)
            .then((response) => {
                const person = response.data.account
                console.log("data api", person)
                this.setState({ person })
                // console.log(JSON.stringify(response.data));
                console.log(person.email)

            })
            .catch(function (error) {
                console.log("Ini errornya", error);
            });
    }

    _approveHandle() {
        var axios = require('axios');

        var config = {
            method: 'post',
            url: 'http://kelompok5.dtstakelompok1.com/admin/account/register/5',
            headers: {
                'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJhZG1pbkBnbWFpbC5jb20ifQ.WtxAH0qixSPf2Q_moYx1v4vIlSwYZIh1aMDFAgn5Q38'
            }
        };

        axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
                console.log("ini ada erorr di config", error);
            });
    }


    _deletHandle() {
        var axios = require('axios');

        var config = {
            method: 'post',
            url: 'http://kelompok5.dtstakelompok1.com/admin/account/delete/3',
            headers: {
                'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJhZG1pbkBnbWFpbC5jb20ifQ.WtxAH0qixSPf2Q_moYx1v4vIlSwYZIh1aMDFAgn5Q38'
            }
        };

        axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    _handleCekBox(event) {
        let arrayid = this.state.ids

        let cekid = event.target.id
        let checked = event.target.checked

        if (checked === true) {
            arrayid.push("id:",parseInt(cekid))
        }

        this.setState({
            arrayid: arrayid
        });
        console.log(arrayid)
        console.log(checked)
    }

    _terimaBanyak() {
        let dataid = this.state.arrayid
        let id = this.state.stringku
        console.log("id dari array", dataid)

        var axios = require('axios');
        var account = dataid
            account.map(account=>JSON.stringify([{account}]))
        console.log("data yg dikirim",account)
        var config = {
            method: 'post',
            url: 'http://kelompok5.dtstakelompok1.com/admin/account/register',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaXNhZG1pbiI6dHJ1ZSwidXNlcm5hbWUiOiJhZG1pbkBnbWFpbC5jb20ifQ.lnORTTQZxHla4B4_ZQ4ZztDnIdlGlsGi9XpCE0VGZ7A'
            },
            id: account
        };

        axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data));
                console.log("data terkirim",config)
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    _deleteBanyak() {
        let dataid = this.state.arrayid
        console.log("id dari array", dataid)

        var axios = require('axios');
        var data = JSON.stringify([{ "id": dataid }]);

        var config = {
            method: 'post',
            url: 'http://kelompok5.dtstakelompok1.com/admin/account/delete',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaXNhZG1pbiI6dHJ1ZSwidXNlcm5hbWUiOiJhZG1pbkBnbWFpbC5jb20ifQ.lnORTTQZxHla4B4_ZQ4ZztDnIdlGlsGi9XpCE0VGZ7A'
            },
            data: data
        };

        axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        // const {person} = this.state
        return (
            <div>
                <NavbarDashboard />
                <div className="container">
                    <div className="card">
                        <div className="card-header">Daftar Register User</div>
                        <div className="card-body">
                            <div className="table-responsive">
                                <button className="btn btn-success mx-2"
                                    onClick={this._terimaBanyak}>Terima</button>
                                <button className="btn btn-danger mx-2"
                                    onClick={this._deleteBanyak}>Tolak</button>


                                <table className="table table-striped table-bordered mt-2">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Aktivasi</th>
                                            <th scope="col">
                                                <input type="checkbox" />
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        {this.state.person.map(person => {
                                            return (
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>{person.email}</td>
                                                    <td>{person.name}</td>
                                                    <td>{person.isapproved ? "Aktive" : "Tolak"}</td>
                                                    <td>
                                                        <button className="btn btn-primary mx-2"
                                                            onClick={this._approveHandle}>
                                                            <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon> Aktif</button>
                                                        <button className="btn btn-danger mx-2"
                                                            onClick={this._deletHandle}>
                                                            <FontAwesomeIcon icon={faBan}></FontAwesomeIcon> Tolak</button>
                                                    </td>
                                                    <td>
                                                        <input
                                                            name="isGoing"
                                                            type="checkbox"
                                                            id={person.ID}
                                                            onChange={this._handleCekBox}
                                                        />
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}