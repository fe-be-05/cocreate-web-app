import React, { Component } from 'react'
import './Trending.css'
import profile from '../../assets/image/profile.png'

export default class TrendingTop extends Component {
    render() {
        return (
            <div>
                <div>
                    <div className="card container-card-tranding-margin">
                        <div className="card-header text-tranding">
                            Trending
                        </div>
                        <ul className="list-group list-group-flush">
                            <li className="list-group-item">Teknologi Informasi IT</li>
                        </ul>
                        <div className="row">
                            <div className="col col-md-2 mt-2 ml-4">
                                <img src={profile} className="image-tranding-style" />
                            </div>
                            <div className="col">
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">
                                        <p className="textTag">Apriansyah</p>
                                        <p className="textTag-info">Teknologi Informasi</p>
                                        <p className="textTag-info-tranding">@apriansya123</p>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-md-2 mt-2 ml-4">
                                <img src={profile} className="image-tranding-style" />
                            </div>
                            <div className="col">
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">
                                        <p className="textTag">Apriansyah</p>
                                        <p className="textTag-info">Teknologi Informasi</p>
                                        <p className="textTag-info-tranding">@apriansya123</p>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-md-2 mt-2 ml-4">
                                <img src={profile} className="image-tranding-style" />
                            </div>
                            <div className="col">
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">
                                        <p className="textTag">Apriansyah</p>
                                        <p className="textTag-info">Teknologi Informasi</p>
                                        <p className="textTag-info-tranding">@apriansya123</p>
                                    </li>
                                </ul>
                            </div>
                        </div>



                        <ul className="list-group list-group-flush">
                            <li className="list-group-item">Financial</li>
                        </ul>
                        <div className="row">
                            <div className="col col-md-2 mt-2 ml-4">
                                <img src={profile} className="image-tranding-style" />
                            </div>
                            <div className="col">
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">
                                        <p className="textTag">Apriansyah</p>
                                        <p className="textTag-info">Teknologi Informasi</p>
                                        <p className="textTag-info-tranding">@apriansya123</p>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-md-2 mt-2 ml-4">
                                <img src={profile} className="image-tranding-style" />
                            </div>
                            <div className="col">
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">
                                        <p className="textTag">Apriansyah</p>
                                        <p className="textTag-info">Teknologi Informasi</p>
                                        <p className="textTag-info-tranding">@apriansya123</p>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-md-2 mt-2 ml-4">
                                <img src={profile} className="image-follow-style" />
                            </div>
                            <div className="col">
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">
                                        <p className="textTag">Apriansyah</p>
                                        <p className="textTag-info">Teknologi Informasi</p>
                                        <p className="textTag-info-follow">@apriansya123</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}