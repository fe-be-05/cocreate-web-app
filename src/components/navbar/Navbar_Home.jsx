import { Component } from 'react'; 
import navbarBrand from '../../assets/image/logo-white.png'
import './Navbar_Home.css';
import { Link, animateScroll as scroll } from "react-scroll";

class Navbar_Home extends Component {

    scrollToTop = () => {
        scroll.scrollToTop();
    };

    render() {
        return (
            <nav className="navbar sticky-top navbar-expand-lg navbar-dashi">
                <div className="container-fluid">
                    <a className="navbar-brand" style={{cursor: "pointer"}}>
                        <img src={navbarBrand} alt="logo-cocreate" onClick={this.scrollToTop}/>
                    </a>

                    <button className="navbar-toggler costum-toggler-home" type="button" data-toggle="collapse" data-target="#navbarHome" aria-controls="navbarHome" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarHome">
                        <ul className="navbar-nav mr-auto">
                        </ul>

                        <ul className="nav navbar-nav navbar-right">
                            <li><Link activeClass="active" to="discover" className="costum-home" spy={true} smooth={true} offset={0} duration={500}>Discover</Link></li>
                            <li><Link activeClass="active" to="feature" className="costum-home" spy={true} smooth={true} offset={0} duration={500}>Features</Link></li>
                            <li><Link activeClass="active" to="our-team" className="costum-home" spy={true} smooth={true} offset={0} duration={500}>Our Team</Link></li>
                            <li><a href="login" className="costum-home">Sign In</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}

export default Navbar_Home