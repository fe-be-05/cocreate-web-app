import react, { Component } from 'react';
import './NavbarDashboard.css'
import navbarBrand from '../../assets/image/CC_1_1.png';

export default class Navbar_Home extends Component{
    render() {
        return (
            <nav className="navbar sticky-top navbar-expand-lg navbar-dash">
                <a className="navbar-brand mx-auto" href="/" style={{display: "flex", justifyContent: "center"}}>
                    <img src={navbarBrand} alt="logo-cocreate" />
                </a>
            </nav>
        )
    }
}