import react, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBell, faHome, faFire, faPlus } from '@fortawesome/free-solid-svg-icons'; 
import navbarBrand from '../../assets/image/CC_1_1.png';
import { connect } from 'react-redux';
import './NavbarDashboard.css';

class Navbar_Dashboard extends Component {
    render() {
        var userLogin = (
            <li className="nav-item active">
                <Link className="nav-link" to="/login">Login</Link>
            </li>
        )
        if (this.props.isLoggedIn) {
            userLogin = (
                <li className="nav-item active">
                    <Link className="nav-link" to="/logout">Logout</Link>
                </li>
            )
        }
        return (
            <div>
                <nav className="navbar sticky-top navbar-expand-lg navbar-dash">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="/dashboard">
                            <img src={navbarBrand} alt="logo-cocreate" />
                        </a>

                        <button className="navbar-toggler costum-toggler" type="button" data-toggle="collapse" data-target="#navbarDashboard" aria-controls="navbarDashboard" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarDashboard">
                            <ul className="navbar-nav mr-auto">
                                <input type="text" className="form-control btn-search" placeholder="Search..."/>
                            </ul>

                            <ul className="navbar-nav navbar-right">
                                <li className="active"><a><Link to="/dashboard"><FontAwesomeIcon className="costum-my-icon" size="2x" icon={faHome}></FontAwesomeIcon></Link></a></li>
                                
                                <li className="active"><a><Link to="/"><FontAwesomeIcon className="costum-my-icon" size="2x" icon={faFire}></FontAwesomeIcon></Link></a></li>

                                <li className="dropdown">
                                    <a className="dropdown" data-toggle="dropdown"><FontAwesomeIcon className="costum-my-icon" size="2x" icon={faPlus}></FontAwesomeIcon>
                                    <span className="caret"></span></a>
                                    <ul className="dropdown-menu dropdown-menu-right sub-menu">
                                        <li className="jdl-dd-nav">Buat</li>
                                        <li className="sub-dd-nav"><Link className="linkedin">Artikel</Link></li>
                                        <li className="sub-dd-nav"><Link className="linkedin" to='/group'>Grup</Link></li>
                                    </ul>
                                </li>
                                
                                <li className="dropdown">
                                    <a className="dropdown" data-toggle="dropdown"><Link><FontAwesomeIcon className="costum-my-icon" size="2x" icon={faBell}></FontAwesomeIcon></Link>
                                    <span className="caret"></span></a>
                                    <ul className="dropdown-menu dropdown-menu-right sub-menu">
                                        <li className="jdl-dd-nav">Notifikasi</li>
                                        <li className="sub-dd-nav">Ini notif</li>
                                    </ul>
                                </li>
                            </ul>

                            <ul className="navbar-nav navbar-right">
                                <li className="dropdown">
                                    <a className="dropdown" data-toggle="dropdown"><img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/fox.jpg" className="rounded-circle avatar"/>
                                    <span className="caret"></span></a>
                                    <ul className="dropdown-menu dropdown-menu-right sub-menu">
                                        <li className="jdl-dd-nav">Profil</li>
                                        <li className="sub-dd-nav"><Link className="linkedin" to="/profileview">Lihat Profil</Link></li>
                                        <li className="sub-dd-nav"><Link className="linkedin" to="/logout">Sign Out</Link></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        isLoggedIn: state.isLoggedIn
    }
}

export default connect(mapStateToProps, null)(Navbar_Dashboard)