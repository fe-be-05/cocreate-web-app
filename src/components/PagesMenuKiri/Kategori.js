import React, { Component } from 'react'
import './PagesMenukiri.css'

export default class KategoriMenuKiri extends Component {
    render() {
        return (
            <div>
                <ul className="list-group list-group-flush">
                    <a className="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Kategory
                            </a>
                    <div className="collapse" id="collapseExample">
                        <div className="card card-body">
                            <div>
                                <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Keuangan</button>
                                <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Otomotif</button>
                                <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Teknologi Informasi / IT</button>

                                <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Sosial</button>
                                <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Olahraga</button>
                                <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Sains</button>
                                <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Desain</button>
                                <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Bisnis dan Manajemen</button>

                                <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Kewirausahan</button>
                                <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Kesehatan</button>
                                <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Energi dan Lingkungan</button>
                                <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 my-2"> Hukum dan Advokasi</button>
                            </div>
                        </div>
                    </div>
                </ul>
            </div>
        )
    }
}