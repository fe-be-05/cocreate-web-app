import React, { Component } from 'react'
import Flat_Design_Home from '../../assets/image/flat-design-home.png'
import { Link } from "react-router-dom";
import './Home.css'
import Navbar_Home from '../navbar/Navbar_Home';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGitlab, faJira } from '@fortawesome/free-brands-svg-icons'

class Home extends Component {
    render() {
        return (
            <div className="styling-my-home">
                <Navbar_Home/>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col col-judul">
                            <h4 className="home-small-title">COMMUNITY</h4>
                            <h1 className="home-title">Collaboration and</h1>
                            <h1 className="home-title">Innovation</h1>
                            <h3 className="home-exp">Gabung dan jadilah bagian dari komunitas terbesar ini</h3>
                            <Link className="nav-link btn btn-register-home" to="/register">Get Started</Link>
                        </div>
                        <div className="col col-gambar">
                            <img src={Flat_Design_Home} className="flat-design"/>
                        </div>
                    </div>

                    <div className="row" id="discover" style={{backgroundColor: "white"}}>
                        <div className="col col-exp-about">
                            <h2 className="judul">About</h2>
                            <p className="exp-abt">CoCreate merupakan platform digital komunitas bagi tim banking dan terbesar di Indonesia. Tagline dari CoCreate yakni kolaborasi dan inovasi</p>
                        </div>
                    </div>

                    <div className="row" id="feature" style={{backgroundColor: "white"}}>
                        <div className="col col-exp-feature">
                            <h2 className="judul" style={{textAlign: "center", marginBottom: "1em"}}>Features</h2>
                            <div className="row">
                                <div className="col">
                                    <h3 className="judul">Kolaborasi CoCreate</h3>
                                    <p className="exp">Anggota CoCreate dapat membuat artikel baru tentang new idea/insight, berita, bahkan proyek inovasi dengan kategori tertentu dengan membuat grup Proyek Inovasi (PI).</p>
                                </div>
                                <div className="col">
                                    <h3 className="judul">Portfolio Anggota CoCreate</h3>
                                    <p className="exp">Anggota CoCreate dapat melihat kontribusi/portfolio anggota lain, menambahkan dan mengubah daftar proyek external, pengetahuan/edukasi/expertise/knowledges, dan pengalaman di portfolio mereka, serta mengunduh portofolio sebagai CV/Resume digital.</p>
                                </div>
                                <div className="col">
                                    <h3 className="judul">CoCreate Innovation Showcases</h3>
                                    <p className="exp">Proyek inovasi yang telah dibuat bisa dipamerkan menggunakan fitur CoCreate Innovation Showcases.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row" id="our-team" style={{backgroundColor: "black", color: "white"}}>
                        <div className="col col-exp-our-team">
                            <h2 className="judul" style={{textAlign: "center", marginBottom: "1em"}}>Our Team</h2>
                            <div className="row">
                                <div className="col">
                                    <h3 className="judul">Front End</h3>
                                    <p className="exp1">Gottfried Christophorus Prasetyadi N.</p>
                                    <p className="exp1">Muhammad Aswar Bakri</p>
                                    <p className="exp1">Aisyah Nurul Hidayah</p>
                                    <p className="exp1">Ryan Apriansyah</p>
                                </div>

                                <div className="col">
                                    <h3 className="judul">Back End</h3>
                                    <p className="exp1">Muhammad Reza Bintami</p>
                                    <p className="exp1">Jupri Eka Pratama</p>
                                    <p className="exp1">Muhammad Zikri</p>
                                    <p className="exp1">Esteria Novebri Simanjuntak</p>
                                </div>
                            </div>
                            <a target="_blank" href="https://gitlab.com/fe-be-05/cocreate-web-app" className="linked-projects"><FontAwesomeIcon size="2x" icon={faGitlab}></FontAwesomeIcon></a>
                            <a target="_blank" href="https://aisyahnrlh.atlassian.net/secure/RapidBoard.jspa?rapidView=3&projectKey=COC&view=planning.nodetail&selectedIssue=COC-7&issueLimit=100" className="linked-projects"><FontAwesomeIcon size="2x" icon={faJira}></FontAwesomeIcon></a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home;