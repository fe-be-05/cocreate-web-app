import { faComment } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { Component } from 'react'
import profile from '../../assets/image/profile.png'
import ai from '../../assets/image/ai.jpeg'
import ps5 from '../../assets/image/ps5.jpeg'
import './Forum.css'
export default class ForumTop extends Component {
    render() {
        return (
            <div>
                <div>
                    <div className="card container-card-Artikel-margin">
                        <div className="card-header text-artikel">
                            Forum
                        </div>
                        <div className="row my-2">
                            <div className="col text-center">
                                <img src={ps5} className="forumImg" />
                            </div>
                            <div className="col">
                                <p className="textForum">Design Studio & Apple</p>
                                <p className="pengirim">Aisyah <em>23 menit lalu</em></p>
                            </div>
                            <div className="col-md-4 text-right">
                                <button className="btn btn-danger btn-sm">Teknologi-IT </button>
                            </div>
                            <div className="col">
                                <p className="textIconForum"><FontAwesomeIcon icon={faComment}></FontAwesomeIcon>32 </p>
                            </div>
                        </div>

                        <div className="row my-1">
                            <div className="col text-center">
                                <img src={profile} className="forumImg" />
                            </div>
                            <div className="col">
                                <p className="textForum">Design Studio & Apple</p>
                                <p className="pengirim">Aisyah <em>23 menit lalu</em></p>
                            </div>
                            <div className="col-md-4 text-right">
                                <button className="btn btn-danger ">Teknologi Informasi-IT </button>
                            </div>
                            <div className="col">
                                <p className="textIconForum"><FontAwesomeIcon icon={faComment}></FontAwesomeIcon>32 </p>
                            </div>
                        </div>

                        <div className="row my-1">
                            <div className="col text-center">
                                <img src={ai} className="forumImg" />
                            </div>
                            <div className="col">
                                <p className="textForum">Design Studio & Apple</p>
                                <p className="pengirim">Aisyah <em>23 menit lalu</em></p>
                            </div>
                            <div className="col-md-4 text-right">
                                <button className="btn btn-danger ">Teknologi Informasi-IT </button>
                            </div>
                            <div className="col">
                                <p className="textIconForum"><FontAwesomeIcon icon={faComment}></FontAwesomeIcon>32 </p>
                            </div>
                        </div>

                        <div className="row my-1">
                            <div className="col text-center">
                                <img src={profile} className="forumImg" />
                            </div>
                            <div className="col">
                                <p className="textForum">Design Studio & Apple</p>
                                <p className="pengirim">Aisyah <em>23 menit lalu</em></p>
                            </div>
                            <div className="col-md-4 text-right">
                                <button className="btn btn-danger ">Teknologi Informasi-IT </button>
                            </div>
                            <div className="col">
                                <p className="textIconForum"><FontAwesomeIcon icon={faComment}></FontAwesomeIcon>32 </p>
                            </div>
                        </div>

                        <div className="row my-1">
                            <div className="col text-center">
                                <img src={ai} className="forumImg" />
                            </div>
                            <div className="col">
                                <p className="textForum">Design Studio & Apple</p>
                                <p className="pengirim">Aisyah <em>23 menit lalu</em></p>
                            </div>
                            <div className="col-md-4 text-right">
                                <button className="btn btn-danger ">Teknologi Informasi-IT </button>
                            </div>
                            <div className="col">
                                <p className="textIconForum"><FontAwesomeIcon icon={faComment}></FontAwesomeIcon>32 </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}